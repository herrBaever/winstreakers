﻿using System.Web.Mvc;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Web.Controllers
{
    public class RootController : Controller
    {
        // GET: Root
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home", new {area = "User"});
        }

    }
}