﻿using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Web.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {

        private String[] _validGeneralActions = {"Index","Create"};
        private String[] _validItemActions = {"Edit","Delete","Details"};

        private String ProduceMatchPattern(String[] matchStrings)
        {
            if (matchStrings.Length >= 2)
            {
                StringBuilder pattern = new StringBuilder();
                pattern.Append("^");
                foreach (string matchString in matchStrings)
                {
                    pattern.Append(matchString);
                    pattern.Append("$|^");
                }
                pattern.Append("$");
                return pattern.ToString();
            }
            else
            {
                return matchStrings.First();
            }
        }

        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            String validGeneralActionsPattern = ProduceMatchPattern(_validGeneralActions);
            String validItemActionsPattern = ProduceMatchPattern(_validItemActions);

            // Home
            context.MapRoute(
                name: "AdminHome",
                url: "Admin/{controller}/{action}",
                defaults: new { controller = "AdminHome", action = "Index" },
                constraints: new { controller = "AdminHome", action = "Index" }
            );

            // Games
            context.MapRoute(
                name: "AdminGames_general",
                url: "Admin/Games/{action}",
                defaults: new { controller = "Games", action = "Index" },
                constraints: new { action = validGeneralActionsPattern }
            );

            context.MapRoute(
                name: "AdminGames_items",
                url: "Admin/Games/{action}/{id}",
                defaults: new { controller = "Games", action = "Index" },
                constraints: new { action = validItemActionsPattern, id = @"\d+" }
            );

            // Matches
            context.MapRoute(
                name: "AdminMatches_general",
                url: "Admin/Matches/{action}",
                defaults: new { controller = "Matches", action = "Index" },
                constraints: new { action = validGeneralActionsPattern }
            );

            context.MapRoute(
                name: "AdminMatches_items",
                url: "Admin/Matches/{action}/{id}",
                defaults: new { controller = "Matches", action = "Index" },
                constraints: new { action = validItemActionsPattern, id = @"\d+" }
            );

            // Players
            context.MapRoute(
                name: "AdminPlayers_general",
                url: "Admin/Players/{action}",
                defaults: new { controller = "Players", action = "Index" },
                constraints: new { action = validGeneralActionsPattern }
            );

            context.MapRoute(
                name: "AdminPlayers_items",
                url: "Admin/Players/{action}/{id}",
                defaults: new { controller = "Players", action = "Index" },
                constraints: new { action = validItemActionsPattern, id = @"\d+" }
            );

            // Teams
            context.MapRoute(
                name: "AdminTeams_general",
                url: "Admin/Teams/{action}",
                defaults: new { controller = "Teams", action = "Index" },
                constraints: new { action = validGeneralActionsPattern }
            );

            context.MapRoute(
                name: "AdminTeams_items",
                url: "Admin/Teams/{action}/{id}",
                defaults: new { controller = "Teams", action = "Index" },
                constraints: new { action = validItemActionsPattern, id = @"\d+" }
            );

            // Tournaments
            context.MapRoute(
                name: "AdminTournaments_general",
                url: "Admin/Tournaments/{action}",
                defaults: new { controller = "Tournaments", action = "Index" },
                constraints: new { action = validGeneralActionsPattern }
            );

            context.MapRoute(
                name: "AdminTournaments_items",
                url: "Admin/Tournaments/{action}/{id}",
                defaults: new { controller = "Tournaments", action = "Index" },
                constraints: new { action = validItemActionsPattern, id = @"\d+" }
            );

        }
    }
}