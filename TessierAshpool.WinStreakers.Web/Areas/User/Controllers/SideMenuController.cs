﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces;
using TessierAshpool.WinStreakers.Web.Areas.User.ViewModels;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Web.Areas.User.Controllers
{
    public class SideMenuController : Controller
    {

        private IGameRepository _gameRepository;

        public SideMenuController(IGameRepository gameRepository)
        {
            _gameRepository = gameRepository;
        }

        public PartialViewResult SideMenu()
        {
            List<MenuItemViewModel> rootCategories = _gameRepository.Games.Select(game => new MenuItemViewModel{
                Category = "Game",
                Name = game.Name,
                MenuItemId = game.GameId,
                MenuDepthLevel = 0
            }).ToList();
            return PartialView(rootCategories);
        }

        public PartialViewResult MenuItems(String category, int id)
        {
            List<MenuItemViewModel> menuData = new List<MenuItemViewModel>();
            String partialView = "_MenuItemsPartial";
            switch (category)
            {
                case "Game":
                {
                    menuData.Add(new MenuItemViewModel { Category = "Tournament", MenuItemId = id, MenuDepthLevel = 1 });
                    menuData.Add(new MenuItemViewModel { Category = "Team", MenuItemId = id, MenuDepthLevel = 1 });
                }
                break;

                case "Tournament":
                {
                    menuData.Add(new MenuItemViewModel { Category = "Region", MenuItemId = id, MenuDepthLevel = 2 });
                    menuData.Add(new MenuItemViewModel { Category = "Match Time", MenuItemId = id, MenuDepthLevel = 2 });
                }
                break;

                case "Team":
                {
                    menuData = _gameRepository.Games
                                              .Where(game => game.GameId == id)
                                              .SelectMany(game => game.Matches
                                              .SelectMany(match => match.TeamsAndOddsForMatches
                                              .Select(teamsAndOdds => new MenuItemViewModel
                                              {
                                                  Category = "MatchesOfTeam",
                                                  MenuItemId = teamsAndOdds.TeamId,
                                                  Name = teamsAndOdds.Team.Name
                                              }))).ToList();
                    partialView = "_LeafMenuItemPartial";
                }
                break;
            }
            return PartialView(partialView, menuData);
        }
    }
}