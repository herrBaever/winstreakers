﻿using System.Linq;
using System.Web.Mvc;
using TessierAshpool.WinStreakers.Business.Entities;
using TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces;
using TessierAshpool.WinStreakers.Web.Areas.User.ViewModels;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Web.Areas.User.Controllers
{
    public class TeamController : Controller
    {

        private ITeamRepository _teamRepository;

        public TeamController(ITeamRepository teamRepository)
        {
            _teamRepository = teamRepository;
        }

        public ViewResult ShowTeam(int id = 1)
        {
            Team currentTeam = _teamRepository.FindById(id);

            return View(new ShowTeamViewModel
            {
                TeamName = currentTeam.Name,
                Members = currentTeam.Members,
                ActiveInGames = currentTeam.TeamsAndOddsForMatches
                                          .Where(team => team.TeamId == id)
                                          .Select(match => match.Match.Game)
                                          .ToArray()
            });
        }

    }
}