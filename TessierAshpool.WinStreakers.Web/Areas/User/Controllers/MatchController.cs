﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TessierAshpool.WinStreakers.Business.Entities;
using TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces;
using TessierAshpool.WinStreakers.Web.Areas.User.ViewModels;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Web.Areas.User.Controllers
{
    public class MatchController : Controller
    {

        private IMatchRepository _matchRepository;
        private ITeamsAndOddsRepository _teamsAndOddsRepository;

        public MatchController(IMatchRepository matchRepository, ITeamsAndOddsRepository teamsAndOddsRepository)
        {
            _matchRepository = matchRepository;
            _teamsAndOddsRepository = teamsAndOddsRepository;
        }

        public PartialViewResult ShowMatch(int id)
        {
            MatchAndTeamViewModel matchAndTeamVM = MatchAndTeamViewModel(_matchRepository.FindById(id));
            return PartialView(matchAndTeamVM);
        }

        public ActionResult ShowWithMatch(Match match)
        {
            MatchAndTeamViewModel matchAndTeamVM = MatchAndTeamViewModel(match);
            return View("ShowMatch", matchAndTeamVM);
        }

        public PartialViewResult ListMatches(Game game)
        {
            List<MatchInListViewModel> matchesInList = game.Matches.Select(match => new MatchInListViewModel
            {
                MatchId = match.MatchId,
                GameName = game.Name,
                StartTime = match.StartTime,
                TeamOneName = match.TeamsAndOddsForMatches.First().Team.Name,
                TeamOneOdds = match.TeamsAndOddsForMatches.First().Odds,
                TeamTwoName = match.TeamsAndOddsForMatches.Last().Team.Name,
                TeamTwoOdds = match.TeamsAndOddsForMatches.Last().Odds,
                DrawOdds = (match.TeamsAndOddsForMatches.First().Odds 
                            + match.TeamsAndOddsForMatches.Last().Odds) * 0.15
            }).ToList();
            return PartialView(matchesInList);
        }
        public ViewResult ListAllMatches()
        {
            return View("ListMatches", _matchRepository.Matches.Select(match => new MatchInListViewModel
            {
                MatchId = match.MatchId,
                StartTime = match.StartTime,
                TeamOneName = match.TeamsAndOddsForMatches.First().Team.Name,
                TeamOneOdds = match.TeamsAndOddsForMatches.First().Odds,
                TeamTwoName = match.TeamsAndOddsForMatches.Last().Team.Name,
                TeamTwoOdds = match.TeamsAndOddsForMatches.Last().Odds,
                DrawOdds = (match.TeamsAndOddsForMatches.First().Odds 
                            + match.TeamsAndOddsForMatches.Last().Odds) * 0.15
            }).ToList());
        }

        public PartialViewResult MatchesOfTeam(int teamId)
        {
            List<MatchInListViewModel> matchesInList = _teamsAndOddsRepository.FindMatchesByTeam(teamId).Select(match => new MatchInListViewModel
            {
                MatchId = match.MatchId,
                StartTime = match.StartTime,
                TeamOneName = match.TeamsAndOddsForMatches.First().Team.Name,
                TeamOneOdds = match.TeamsAndOddsForMatches.First().Odds,
                TeamTwoName = match.TeamsAndOddsForMatches.Last().Team.Name,
                TeamTwoOdds = match.TeamsAndOddsForMatches.Last().Odds,
                DrawOdds = (match.TeamsAndOddsForMatches.First().Odds
                            + match.TeamsAndOddsForMatches.Last().Odds) * 0.15
            }).ToList();
            return PartialView("ListMatches", matchesInList);
        }

        private MatchAndTeamViewModel MatchAndTeamViewModel(Match match)
        {
            MatchAndTeamViewModel matchAndTeamVM = new MatchAndTeamViewModel
            {
                Match = match,
                Teams = _teamsAndOddsRepository.FindTeamsByMatch(match.MatchId)
            };
            return matchAndTeamVM;
        }

    }
}