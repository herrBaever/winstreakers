﻿using System.Linq;
using System.Web.Mvc;
using TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Web.Areas.User.Controllers
{
    public class GameController : Controller
    {

        private IGameRepository _gameRepository;

        public GameController(IGameRepository gameRepository)
        {
            _gameRepository = gameRepository;
        }

        public ViewResult ShowGame(int id = 1)
        {
            return View(_gameRepository.FindById(id));
        }

        public ViewResult ListGames()
        {
            return View(_gameRepository.Games.OrderBy(game => game.Name));
        }

    }
}