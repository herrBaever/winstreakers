﻿using System.Web.Mvc;
using TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Web.Areas.User.Controllers
{
    public class PlayerController : Controller
    {

        private IPlayerRepository _playerRepository;

        public PlayerController(IPlayerRepository playerRepository)
        {
            _playerRepository = playerRepository;
        }

        public ViewResult ShowPlayer(int id = 1)
        {
            return View(_playerRepository.FindById(id));
        }

        public ViewResult ListPlayers()
        {
            return View(_playerRepository.Players);
        }

    }
}