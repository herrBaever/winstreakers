﻿using System.Web.Mvc;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Web.Areas.User
{
    public class UserAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "User";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {

            context.MapRoute(
                name: "gameBy_Id",
                url: "Game/ShowGame/{id}",
                defaults: new { controller = "Game", action = "ShowGame" }
            );

            context.MapRoute(
                name: "matches_by_teams",
                url: "Match/MatchesOfTeam/{teamId}",
                defaults: new { controller = "Match", action = "MatchesOfTeam" }
            );

            context.MapRoute(
                name: "showMatch",
                url: "Match/ShowMatch/{id}",
                defaults: new { controller = "Match", action = "ShowMatch" }
            );

            context.MapRoute(
                name: "player_by_Id",
                url: "Player/ShowPlayer/{id}",
                defaults: new { controller = "Player", action = "ShowPlayer" }
            );

            context.MapRoute(
                name: "team_by_Id",
                url: "Team/ShowTeam/{id}",
                defaults: new { controller = "Team", action = "ShowTeam" }
            );

            context.MapRoute(
                name: "sideMenu",
                url: "SideMenu/MenuItems/{category}/{id}",
                defaults: new { controller = "SideMenu", action = "MenuItems" }
            );

            context.MapRoute(
                name: "User_default",
                url: "User/{controller}/{action}/{id}",
                defaults: new { action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}