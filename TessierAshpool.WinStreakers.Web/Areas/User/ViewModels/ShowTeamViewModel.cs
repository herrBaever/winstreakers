﻿using System;
using System.Collections.Generic;
using TessierAshpool.WinStreakers.Business.Entities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Web.Areas.User.ViewModels
{
    public class ShowTeamViewModel
    {
        public String TeamName { get; set; }
        public IEnumerable<Player> Members { get; set; }
        public IEnumerable<Game> ActiveInGames { get; set; }

    }
}