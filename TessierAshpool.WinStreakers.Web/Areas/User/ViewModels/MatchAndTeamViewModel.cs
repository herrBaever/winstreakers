﻿using System.Collections.Generic;
using TessierAshpool.WinStreakers.Business.Entities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Web.Areas.User.ViewModels
{
    public class MatchAndTeamViewModel
    {
        public Match Match { get; set; }
        public IEnumerable<Team> Teams { get; set; }
    }
}