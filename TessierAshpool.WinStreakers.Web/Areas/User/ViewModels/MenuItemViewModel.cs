﻿using System;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Web.Areas.User.ViewModels
{
    public class MenuItemViewModel
    {
        public String Category { get; set; }
        public String Name { get; set; }
        public int MenuItemId { get; set; }
        public int MenuDepthLevel { get; set; }
    }
}