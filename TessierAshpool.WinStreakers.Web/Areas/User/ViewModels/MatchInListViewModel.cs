﻿using System;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Web.Areas.User.ViewModels
{
    public class MatchInListViewModel
    {
        public int MatchId { get; set; }
        public String GameName { get; set; }
        public DateTime StartTime { get; set; }

        public String TeamOneName { get; set; }
        public double TeamOneOdds { get; set; }

        public String TeamTwoName { get; set; }
        public double TeamTwoOdds { get; set; }

        public double DrawOdds { get; set; }

    }
}