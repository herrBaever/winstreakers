﻿
// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Web.Areas.User.ViewModels
{
    public class GameInViewInfo
    {
        public int NumberOfGames { get; set; }
        public int GameInView { get; set; }
    }
}