﻿// <author>Jonas Benzon Illum</author>

function updateMatchContent(event) {
    var $controllerUrl = '/Match/'
        + event.data.controllerAction + '/'
        + event.data.id;
    $.ajax({
        url: $controllerUrl,
        contentType: 'application/html; charset=utf-8',
        type: 'GET',
        dataType: 'html'
    })
        .success(function(result) {
            $(event.data.receivingContainer).html(result);
        });

    $('.matchRow').one("click", function () {
        $('#selectedMatch-root').css({ "visibility": "visible" });
    });

}