﻿// <author>Jonas Benzon Illum</author>

function openSubMenu(event) {
    var $controllerUrl = '/SideMenu/MenuItems/' 
        + event.data.category + '/'
        + event.data.gameId;
    $(this).toggleClass("open");
    var $subMenuIdentity = "#" + event.data.category + event.data.gameId + ".subMenuContent";
    var $subMenu = $(this).parent().siblings($subMenuIdentity);
    $.ajax({
        url: $controllerUrl,
        contentType: 'application/html; charset=utf-8',
        type: 'GET',
        dataType: 'html'
    })
        .success(function (result) {
            $subMenu.html(result);
            $subMenu.toggle();
        });
}
