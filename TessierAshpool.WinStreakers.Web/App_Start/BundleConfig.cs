﻿using System.Web.Optimization;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Web
{
    public class BundleConfig
    {

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add( 
                new StyleBundle("~/bundles/css")
                .Include(
                "~/Content/Css/bootstrap.css",
                "~/Content/Css/sideMenu.css",
                "~/Content/Css/home.css",
                "~/Content/Css/matchList.css",
                "~/Content/Css/yetiThemeNavbar.css"
            ));

            bundles.Add(
                new ScriptBundle("~/bundles/jquery")
                .Include(
                "~/Scripts/jquery-{version}.js"
            ));

            bundles.Add(
                new ScriptBundle("~/bundles/bootstrap")
                .Include(
                "~/Scripts/bootstrap.js"
            ));

            bundles.Add(
                new ScriptBundle("~/bundles/custom")
                .Include(
                "~/Scripts/Custom/updateMatchContent.js",
                "~/Scripts/Custom/openSubMenu.js"
            ));

        }
        
    }
    
}