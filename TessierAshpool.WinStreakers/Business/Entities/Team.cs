﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Business.Entities
{
    public class Team
    {
        public int TeamId { get; set; }
        [Required]
        public String Name { get; set; }
        public virtual ICollection<TeamOddsForMatch> TeamsAndOddsForMatches { get; set; }
        public virtual ICollection<Player> Members { get; set; }
    }
}
