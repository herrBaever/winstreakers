﻿using System;
using System.ComponentModel.DataAnnotations;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Business.Entities
{
    public class Player
    {
        public int PlayerId { get; set; }
        [Required]
        public String Name { get; set; }

        public int TeamId { get; set; }
        public virtual Team Team { get; set; }
    }
}
