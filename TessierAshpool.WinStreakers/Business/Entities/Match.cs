﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Business.Entities
{
    public class Match
    {
        [Required]
        public int MatchId { get; set; }

        public String Description { get; set; }

        [Required]
        public DateTime StartTime { get; set; }

        public virtual ICollection<TeamOddsForMatch> TeamsAndOddsForMatches { get; set; }

        [Required]
        public int GameId { get; set; }

        public virtual Game Game{ get; set; }

        public int? TournamentId { get; set; } // Allowing null fk.
        public virtual Tournament Tournament { get; set; }

    }
}
