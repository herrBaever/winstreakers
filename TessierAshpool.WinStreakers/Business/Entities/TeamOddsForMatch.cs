﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Business.Entities
{
    public class TeamOddsForMatch
    {
        [Key]
        [Column(Order = 0)]
        public int TeamId { get; set; }
        
        public virtual Team Team { get; set; }

        [Key]
        [Column(Order = 1)]
        public int MatchId { get; set; }
        public virtual Match Match { get; set; }
        [Required]
        public double Odds { get; set; }
    }
}
