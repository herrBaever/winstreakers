﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Business.Entities
{
    public class Game
    {
        [Required]
        public int GameId { get; set; }
        [Required]
        public String Name { get; set; }
        public String Info { get; set; }

        public virtual ICollection<Match> Matches { get; set; }

    }
}
