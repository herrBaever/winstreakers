﻿using System;
using System.ComponentModel.DataAnnotations;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.Business.Entities
{
    public enum Region
    {
        Asien,
        Danmark,
        Europa,
        USA
    }

   public class Tournament
    {
        public int TournamentId { get; set; }
        [Required]
        public String Name { get; set; }
        [Required]
        public DateTime StartTime { get; set; }
        [Required]
        public DateTime EndTime { get; set; }
        [Required]
        public Region Region { get; set; }
    }
}
