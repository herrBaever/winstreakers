﻿using System.Data.Entity;
using TessierAshpool.WinStreakers.Business.Entities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Utilities
{
    public class WinStreakersDbContext : DbContext
    {
        public DbSet<Game> Games { get; set; }
        public DbSet<Match> Matches { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<TeamOddsForMatch> TeamsAndOddsForMatches { get; set; }
        public DbSet<Tournament> Tournaments { get; set; }
    }
}
