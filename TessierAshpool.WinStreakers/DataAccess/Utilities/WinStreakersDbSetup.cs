﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using TessierAshpool.WinStreakers.Business.Entities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Utilities
{
    public class WinStreakersDbSetup : System.Data.Entity.DropCreateDatabaseIfModelChanges<WinStreakersDbContext> // To force a drop, use .DropCreateDatabaseAlways<... once, then change back .DropCreateDatabaseIfModelChanges
    {

        protected override void Seed(WinStreakersDbContext context)
        {
            SeedGames(context);
            SeedTournaments(context);
            SeedTeams(context);
            SeedPlayers(context);
            SeedMatches(context);
        }

        private void SeedTeams(WinStreakersDbContext context)
        {
            var teams = new List<Team>
            {
                new Team{ TeamId = 1, Name = "1990: The Bronx Warriors"},
                new Team{ TeamId = 2, Name = "Samurai Cops"},
                new Team{ TeamId = 3, Name = "Santas Summer Vacation"},
                new Team{ TeamId = 4, Name = "Olsen-Banden"},
                new Team{ TeamId = 5, Name = "Batman Rejects"},
                new Team{ TeamId = 6, Name = "Militant Wasps"},
                new Team{ TeamId = 7, Name = "Silent Scorpions"},
                new Team{ TeamId = 8, Name = "Reptile Mavericks"},
                new Team{ TeamId = 9, Name = "Mighty Tigers"},
                new Team{ TeamId = 10, Name = "Extreme Sonics"},
                new Team{ TeamId = 11, Name = "Butterfly Rebels"},
                new Team{ TeamId = 12, Name = "Skull Widows"},
                new Team{ TeamId = 13, Name = "Alpha Fireballs"},
                new Team{ TeamId = 14, Name = "Extreme Panthers"},
                new Team{ TeamId = 15, Name = "Rocky Dreamers"},
                new Team{ TeamId = 16, Name = "Striking Slayers"},
                new Team{ TeamId = 17, Name = "The Cajun Hot Sticks"},
                new Team{ TeamId = 18, Name = "The Cheeky Monkeys"},
                new Team{ TeamId = 19, Name = "Norfolk-in-Chance"},
                new Team{ TeamId = 20, Name = "Cow Tipping Dwarfs"},
                new Team{ TeamId = 21, Name = "Victorious Secret"},
                new Team{ TeamId = 22, Name = "The Cereal Killers"},
                new Team{ TeamId = 23, Name = "Wii Not Fit"},
                new Team{ TeamId = 24, Name = "Here for Beer"},
                new Team{ TeamId = 25, Name = "Wacky Waving Inflatable Flailing Arm Tube Men"},
                new Team{ TeamId = 26, Name = "Fire Breathing Rubber Duckies"},
                new Team{ TeamId = 27, Name = "Cuban Raft Riders"},
                new Team{ TeamId = 28, Name = "Designated Drinkers"},
                new Team{ TeamId = 29, Name = "Norfolk-in-Chance"},
                new Team{ TeamId = 30, Name = "Cow Tipping Dwarfs"},
                new Team{ TeamId = 31, Name = "Viscious and Delicious"},
                new Team{ TeamId = 32, Name = "e-LEMON-ators"}
            };
            teams.ForEach(team => context.Teams.AddOrUpdate(team));
            context.SaveChanges();
        }


        private void SeedGames(WinStreakersDbContext context)
        {
            var games = new List<Game>
            {
                new Game {
                    GameId = 1,
                    Name = "Team Fortress II",
                    Info = "A game where the best hat wins"},
                new Game
                {
                    GameId = 2,
                    Name = "Cykelmyggen Magnus",
                    Info = "How this game became one of the top competetives, remains unknown"
                },
                new Game
                {
                    GameId = 3,
                    Name = "Baldurs Gate",
                    Info = "Fetch your ring of wizardry Elminster!"
                },
                new Game
                {
                    GameId = 4,
                    Name = "Carmageddon",
                    Info = "Open doors for bonus points"
                }
            };
            games.ForEach(game => context.Games.AddOrUpdate(game));
            context.SaveChanges();
        }


        private void SeedTournaments(WinStreakersDbContext context)
        {
            var tournaments = new List<Tournament>
            {
                new Tournament
                {
                    Name = "DreamHack",
                    StartTime = new DateTime(2014, 11, 18, 12, 30, 0),
                    EndTime = new DateTime(2014, 11, 23, 23, 0, 0),
                    Region = Region.Europa
                },
                new Tournament
                {
                    Name = "MuchPSU",
                    StartTime = new DateTime(2014, 11, 16, 10, 15, 0),
                    EndTime = new DateTime(2014, 11, 19, 23, 59, 0),
                    Region = Region.Asien
                },
                new Tournament
                {
                    Name = "HallOfSweat",
                    StartTime = new DateTime(2014, 12, 22, 20, 8, 0),
                    EndTime = new DateTime(2014, 12, 26, 20, 12, 30),
                    Region = Region.Danmark
                },
            };
            tournaments.ForEach(tournament => context.Tournaments.AddOrUpdate(tournament));
            context.SaveChanges();
        }


        private void SeedMatches(WinStreakersDbContext context)
        {
            var matches = new List<Match>
            {
                new Match
                {   
                    MatchId = 1,
                    Description = "Time has come",
                    GameId = 1,
                    StartTime = new DateTime(2014, 11, 20, 21, 30, 0),
                    TournamentId = 1
                },
                new Match
                {   
                    MatchId =  2,
                    Description = "Fight!",
                    GameId = 1,
                    StartTime = new DateTime(2014, 11, 22, 20, 30, 0),
                },
                new Match
                {   
                    MatchId =  3,
                    Description = "Who goes hatless?",
                    GameId = 1,
                    StartTime = new DateTime(2014, 11, 26, 12, 15, 0),
                },
                new Match
                {   
	                MatchId =  4,
                    Description = "Golden hat at stake",
                    GameId = 1,
                    StartTime = new DateTime(2014, 12, 24, 20, 20, 0),
                    TournamentId = 3
                },
                new Match
                {   
			        MatchId =  5,
                    Description = "Quarterfinal A",
                    GameId = 2,
                    StartTime = new DateTime(2014, 11, 20, 17, 30, 0),
                },
                new Match
                {   
					MatchId =  6,
                    Description = "Quarterfinal B",
                    GameId = 2,
                    StartTime = new DateTime(2014, 11, 20, 17, 30, 0),
                },
                new Match
                {   
                    MatchId = 7,
                    Description = "Quarterfinal C",
                    GameId = 2,
                    StartTime = new DateTime(2014, 11, 20, 17, 30, 0),
                },
                new Match
                {   
                    MatchId =  8,
                    Description = "Quarterfinal D",
                    GameId = 2,
                    StartTime = new DateTime(2014, 11, 20, 17, 30, 0),
                },
                new Match
                {   
                    MatchId =  9,
                    Description = "Semifinal A",
                    GameId = 2,
                    StartTime = new DateTime(2014, 11, 25, 19, 10, 0),
                },
                new Match
                {   
                    MatchId =  10,
                    Description = "Semifinal B",
                    GameId = 2,
                    StartTime = new DateTime(2014, 11, 25, 19, 10, 0),
                },
                new Match
                {   
                    MatchId =  11,
                    Description = "Final",
                    GameId = 2,
                    StartTime = new DateTime(2014, 12, 1, 22, 00, 0),
                },
                new Match
                {   
                    MatchId =  12,
                    Description = "Turn undead versus bard song",
                    GameId = 3,
                    StartTime = new DateTime(2014, 12, 1, 9, 00, 0),
                },
                new Match
                {   
                    MatchId =  13,
                    Description = "Will boots of speed affect the outcome?",
                    GameId = 3,
                    StartTime = new DateTime(2015, 1, 7, 5, 30, 0),
                },
                new Match
                {   
                    MatchId =  14,
                    Description = "All cars have fresh tires",
                    GameId = 4,
                    StartTime = new DateTime(2014, 11, 19, 1, 30, 0),
                    TournamentId = 2
                },
                new Match
                {  
                    MatchId =  15,
                    Description = "Car wash price jump!",
                    GameId = 4,
                    StartTime = new DateTime(2014, 11, 20, 13, 3, 30),
                },
                new Match
                {   
                    MatchId =  16,
                    Description = "Who bend my antenna?",
                    GameId = 4,
                    StartTime = new DateTime(2014, 11, 20, 15, 30, 0),
                }
            };
            matches.ForEach(match => context.Matches.AddOrUpdate(match));
            context.SaveChanges();

            // For now, each match just have two teams, but it could posible have more later
            var teamsAndOdds = new List<TeamOddsForMatch>
            {
                new TeamOddsForMatch { MatchId = 1, TeamId = 6, Odds = 2.01 },
                new TeamOddsForMatch { MatchId = 2, TeamId = 4, Odds = 0.50 },
                new TeamOddsForMatch { MatchId = 3, TeamId = 12, Odds = 3.16 },
                new TeamOddsForMatch { MatchId = 4, TeamId = 32, Odds = 0.70 },
                new TeamOddsForMatch { MatchId = 5, TeamId = 1, Odds = 1.33 },
                new TeamOddsForMatch { MatchId = 6, TeamId = 4, Odds = 2.48 },
                new TeamOddsForMatch { MatchId = 7, TeamId = 11, Odds = 4.98 },
                new TeamOddsForMatch { MatchId = 8, TeamId = 22, Odds = 2.45 },
                new TeamOddsForMatch { MatchId = 9, TeamId = 10, Odds = 0.79 },
                new TeamOddsForMatch { MatchId = 10, TeamId = 3, Odds = 1.12 },
                new TeamOddsForMatch { MatchId = 11, TeamId = 28, Odds = 2.78 },
                new TeamOddsForMatch { MatchId = 12, TeamId = 9, Odds = 2.45 },
                new TeamOddsForMatch { MatchId = 13, TeamId = 3, Odds = 4.87 },
                new TeamOddsForMatch { MatchId = 14, TeamId = 23, Odds = 2.98 },
                new TeamOddsForMatch { MatchId = 15, TeamId = 15, Odds = 1.54 },
                new TeamOddsForMatch { MatchId = 16, TeamId = 13, Odds = 0.68 },

                new TeamOddsForMatch { MatchId = 1, TeamId = 1, Odds = 2.44 },
                new TeamOddsForMatch { MatchId = 2, TeamId = 5, Odds = 3.65 },
                new TeamOddsForMatch { MatchId = 3, TeamId = 16, Odds = 5.98 },
                new TeamOddsForMatch { MatchId = 4, TeamId = 19, Odds = 4.78 },
                new TeamOddsForMatch { MatchId = 5, TeamId = 30, Odds = 3.74 },
                new TeamOddsForMatch { MatchId = 6, TeamId = 21, Odds = 1.65 },
                new TeamOddsForMatch { MatchId = 7, TeamId = 10, Odds = 1.37 },
                new TeamOddsForMatch { MatchId = 8, TeamId = 7, Odds = 3.47 },
                new TeamOddsForMatch { MatchId = 9, TeamId = 16, Odds = 0.74 },
                new TeamOddsForMatch { MatchId = 10, TeamId = 5, Odds = 0.12 },
                new TeamOddsForMatch { MatchId = 11, TeamId = 31, Odds = 9.65 },
                new TeamOddsForMatch { MatchId = 12, TeamId = 1, Odds = 1.58 },
                new TeamOddsForMatch { MatchId = 13, TeamId = 10, Odds = 2.91 },
                new TeamOddsForMatch { MatchId = 14, TeamId = 3, Odds = 3.64 },
                new TeamOddsForMatch { MatchId = 15, TeamId = 18, Odds = 1.00 },
                new TeamOddsForMatch { MatchId = 16, TeamId = 2, Odds = 4.75 }
            };
            teamsAndOdds.ForEach(teamAndOdds => context.TeamsAndOddsForMatches.AddOrUpdate(teamAndOdds));
            context.SaveChanges();
        }


        private void SeedPlayers(WinStreakersDbContext context)
        {
            var players = new List<Player>
            {
                new Player{ Name = "Søren", TeamId = 1},
                new Player{ Name = "Bjarke", TeamId = 1},
                new Player{ Name =  "Ursula", TeamId = 1},
                new Player{ Name = "Signe", TeamId = 2},
                new Player{ Name = "Troggdor", TeamId = 2},
                new Player{ Name =  "Niels", TeamId = 2},
                new Player{ Name = "Killah1337", TeamId = 2},
                new Player{ Name = "DeathPower2001", TeamId = 2},
                new Player{ Name =  "Støvring", TeamId = 2},
                new Player{ Name = "Anne-Marie", TeamId = 3},
                new Player{ Name = "Buksti", TeamId = 3},
                new Player{ Name =  "Peter Petersen", TeamId = 4},
                new Player{ Name = "Dragsted", TeamId = 4},
                new Player{ Name = "Lykkeberg", TeamId = 4},
                new Player{ Name =  "Pundik", TeamId = 4},
                new Player{ Name = "Lidegaard", TeamId = 5},
                new Player{ Name = "Ditte Giese", TeamId = 5},
                new Player{ Name =  "Nordest", TeamId = 5},

                // And now I'm out of ideas. Pokemon name generator to the rescue!
                new Player{ Name = "Doltemon", TeamId = 6},
                new Player{ Name = "Inaezard", TeamId = 6},
                new Player{ Name = "Dipirtle", TeamId = 6},
                new Player{ Name = "Anichu", TeamId = 6},
                new Player{ Name = "Itazard", TeamId = 6},
                new Player{ Name = "Muckachu", TeamId = 6},
                new Player{ Name = "Fingeramon", TeamId = 7},
                new Player{ Name = "Snarkurtle", TeamId = 7},
                new Player{ Name = "Munchamon", TeamId = 7},
                new Player{ Name = "Tiayzard", TeamId = 7},
                new Player{ Name = "Onyzard", TeamId = 7},
                new Player{ Name = "Thimbleartle", TeamId = 7},
                new Player{ Name = "Ballemon", TeamId = 8},
                new Player{ Name = "Erezard", TeamId = 8},
                new Player{ Name = "Cornazard", TeamId = 8},
                new Player{ Name = "Twitomon", TeamId = 8},
                new Player{ Name = "Faceozard", TeamId = 8},
                new Player{ Name = "Dolturtle", TeamId = 8},
                new Player{ Name = "Bumizard", TeamId = 9},
                new Player{ Name = "Bumpochu", TeamId = 9},
                new Player{ Name = "Rodichu", TeamId = 9},
                new Player{ Name = "Wipeertle", TeamId = 10},
                new Player{ Name = "Wipeymon", TeamId = 10},
                new Player{ Name = "Imimon", TeamId = 10},
                new Player{ Name = "Beefamon", TeamId = 10},
                new Player{ Name = "Oldortle", TeamId = 10},
                new Player{ Name = "Therezard", TeamId = 11},
                new Player{ Name = "Puffachu", TeamId = 11},
                new Player{ Name = "Kalachu", TeamId = 11},
                new Player{ Name = "Ormomon", TeamId = 11},
                new Player{ Name = "Omomon", TeamId = 12},
                new Player{ Name = "Munchemon", TeamId = 12},
                new Player{ Name = "Yerizard", TeamId = 12},
                new Player{ Name = "Arazard", TeamId = 12},
                new Player{ Name = "Enizard", TeamId = 13},
                new Player{ Name = "Sayamon", TeamId = 13},
                new Player{ Name = "Clownozard", TeamId = 13},
                new Player{ Name = "Dumbachu", TeamId = 13},
                new Player{ Name = "Oromon", TeamId = 13},
                new Player{ Name = "Garychu", TeamId = 13},
                new Player{ Name = "Voryzard", TeamId = 14},
                new Player{ Name = "Oldezard", TeamId = 14},
                new Player{ Name = "Cheeseachu", TeamId = 15},
                new Player{ Name = "Eldimon", TeamId = 15},
                new Player{ Name = "Twitertle", TeamId = 15},
                new Player{ Name = "Essechu", TeamId = 15},
                new Player{ Name = "Wipeyrtle", TeamId = 15},
                new Player{ Name = "Iamon", TeamId = 16},
                new Player{ Name = "Doltechu", TeamId = 16},
                new Player{ Name = "Twerpazard", TeamId = 16},
                new Player{ Name = "Muckimon", TeamId = 16},
                new Player{ Name = "Boneachu", TeamId = 16},
                new Player{ Name = "Ardirtle", TeamId = 17},
                new Player{ Name = "Oughezard", TeamId = 17},
                new Player{ Name = "Pinechu", TeamId = 17},
                new Player{ Name = "Twityzard", TeamId = 17},
                new Player{ Name = "Twitemon", TeamId = 18},
                new Player{ Name = "Bururtle", TeamId = 18},
                new Player{ Name = "Danuzard", TeamId = 18},
                new Player{ Name = "Elmimon", TeamId = 18},
                new Player{ Name = "Ineumon", TeamId = 19},
                new Player{ Name = "Pinumon", TeamId = 19},
                new Player{ Name = "Issachu", TeamId = 19},
                new Player{ Name = "Sayamon", TeamId = 19},
                new Player{ Name = "Onyrtle", TeamId = 19},
                new Player{ Name = "Sneezeechu", TeamId = 20},
                new Player{ Name = "Twerpyzard", TeamId = 20},
                new Player{ Name = "Torortle", TeamId = 20},
                new Player{ Name = "Lumpimon", TeamId = 20},
                new Player{ Name = "Beefuchu", TeamId = 21},
                new Player{ Name = "Ankleazard", TeamId = 21},
                new Player{ Name = "Sneezeozard", TeamId = 21},
                new Player{ Name = "Knuckleazard", TeamId = 21},
                new Player{ Name = "Warazard", TeamId = 21},
                new Player{ Name = "Achozard", TeamId = 22},
                new Player{ Name = "Osartle", TeamId = 22},
                new Player{ Name = "Boneazard", TeamId = 23},
                new Player{ Name = "Tinuchu", TeamId = 23},
                new Player{ Name = "Clodizard", TeamId = 23},
                new Player{ Name = "Wipeurtle", TeamId = 23},
                new Player{ Name = "Thimbleochu", TeamId = 24},      
                new Player{ Name = "Orochu", TeamId = 24},
                new Player{ Name = "Clotuzard", TeamId = 24},
                new Player{ Name = "Essimon", TeamId = 24},
                new Player{ Name = "Cheeseuzard", TeamId = 25},
                new Player{ Name = "Rayuchu", TeamId = 25},
                new Player{ Name = "Achuchu", TeamId = 25},
                new Player{ Name = "Wipeachu", TeamId = 25},
                new Player{ Name = "Doofizard", TeamId = 26},
                new Player{ Name = "Worumon", TeamId = 26},
                new Player{ Name = "Athymon", TeamId = 26},
                new Player{ Name = "Kinamon", TeamId = 26},
                new Player{ Name = "Yeruzard", TeamId = 27},
                new Player{ Name = "Twitozard", TeamId = 27},
                new Player{ Name = "Nitozard", TeamId = 27},
                new Player{ Name = "Cheeseirtle", TeamId = 27},
                new Player{ Name = "Essezard", TeamId = 28},
                new Player{ Name = "Beefizard", TeamId = 28},
                new Player{ Name = "Athachu", TeamId = 28},
                new Player{ Name = "Grumbleimon", TeamId = 29},
                new Player{ Name = "Nityzard", TeamId = 29},
                new Player{ Name = "Torechu", TeamId = 29}
            };
            players.ForEach(player => context.Players.AddOrUpdate(player));
            context.SaveChanges();
        }
    }
}
