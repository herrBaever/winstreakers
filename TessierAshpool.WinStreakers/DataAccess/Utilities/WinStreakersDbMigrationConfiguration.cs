using System.Data.Entity.Migrations;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Utilities
{
    internal sealed class WinStreakersDbMigrationConfiguration : DbMigrationsConfiguration<WinStreakersDbContext>
    {
        public WinStreakersDbMigrationConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "TessierAshpool.WinStreakers.DataAccess.WinStreakersDbContext";
        }

    }
}
