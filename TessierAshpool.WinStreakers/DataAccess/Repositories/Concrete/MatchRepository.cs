﻿using System.Collections.Generic;
using System.Linq;
using TessierAshpool.WinStreakers.Business.Entities;
using TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces;
using TessierAshpool.WinStreakers.DataAccess.Utilities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Repositories.Concrete
{
    public class MatchRepository : IMatchRepository
    {

        private WinStreakersDbContext _context = new WinStreakersDbContext();

        public IEnumerable<Match> Matches 
        {
            get { return _context.Matches.ToList(); }
        }

        public IEnumerable<Match> MatchesForGame(int gameId)
        {        
            return _context.Matches.Where(match => match.GameId == gameId).ToList();
        }

        public Match FindById(int id)
        {
            return _context.Matches.Find(id);
        }

    }
}
