﻿using System;
using System.Collections.Generic;
using System.Linq;
using TessierAshpool.WinStreakers.Business.Entities;
using TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces;
using TessierAshpool.WinStreakers.DataAccess.Utilities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Repositories.Concrete
{
    public class TournamentRepository : ITournamentRepository
    {

        private WinStreakersDbContext _context = new WinStreakersDbContext();

        public IEnumerable<Tournament> Tournaments
        {
            get { return _context.Tournaments; }
        }

        public IEnumerable<Tournament> FindTournamentsInTimeRange(DateTime startTime, DateTime endTime)
        {
            return from tournament in _context.Tournaments
                where tournament.StartTime < endTime && tournament.EndTime > startTime
                select tournament;
        }

        public IEnumerable<Tournament> FindTournamentsInRegion(Region region)
        {
           return _context.Tournaments.Where(tournament => tournament.Region == region).ToList();
        }


    }
}
