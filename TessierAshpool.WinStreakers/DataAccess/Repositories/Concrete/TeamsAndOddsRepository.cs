﻿using System.Collections.Generic;
using System.Linq;
using TessierAshpool.WinStreakers.Business.Entities;
using TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces;
using TessierAshpool.WinStreakers.DataAccess.Utilities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Repositories.Concrete
{
    public class TeamsAndOddsRepository : ITeamsAndOddsRepository
    {
        private WinStreakersDbContext _context = new WinStreakersDbContext();

        public IEnumerable<TeamOddsForMatch> TeamsAndOddsForMatches
        {
            get { return _context.TeamsAndOddsForMatches; }
        }

        public IEnumerable<Team> FindTeamsByMatch(int matchId)
        {
            var teams = _context.Teams
                .Where(team => team.TeamsAndOddsForMatches
                .Any(match => match.MatchId == matchId));
            return teams;
        }

        public IEnumerable<Match> FindMatchesByTeam(int teamId)
        {
            return _context.Matches
                .Where(match => match.TeamsAndOddsForMatches
                .Any(team => team.TeamId == teamId)).ToList();
        }

    }
}
