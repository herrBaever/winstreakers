﻿using System.Collections.Generic;
using System.Linq;
using TessierAshpool.WinStreakers.Business.Entities;
using TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces;
using TessierAshpool.WinStreakers.DataAccess.Utilities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Repositories.Concrete
{
    public class GameRepository : IGameRepository
    {

        private WinStreakersDbContext _context = new WinStreakersDbContext();

        public IEnumerable<Game> Games
        {
            get { return _context.Games.ToList(); }
        }

        public Game FindById(int id)
        {
            return _context.Games.Find(id);
        }

    }
}
