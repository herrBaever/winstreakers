﻿using System.Collections.Generic;
using System.Linq;
using TessierAshpool.WinStreakers.Business.Entities;
using TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces;
using TessierAshpool.WinStreakers.DataAccess.Utilities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Repositories.Concrete
{
    public class PlayerRepository : IPlayerRepository
    {

        private WinStreakersDbContext _context = new WinStreakersDbContext();

        public IEnumerable<Player> Players
        {
            get { return _context.Players.ToList(); }
        }

        public Player FindById(int id)
        {
            return _context.Players.Find(id);
        }

    }
}
