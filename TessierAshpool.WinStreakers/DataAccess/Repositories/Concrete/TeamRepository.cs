﻿using System.Collections.Generic;
using TessierAshpool.WinStreakers.Business.Entities;
using TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces;
using TessierAshpool.WinStreakers.DataAccess.Utilities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Repositories.Concrete
{
    public class TeamRepository : ITeamRepository
    {

        private WinStreakersDbContext _context = new WinStreakersDbContext();

        public IEnumerable<Team> Teams
        {
            get { return _context.Teams; }
        }

        public Team FindById(int id)
        {
            return _context.Teams.Find(id);
        }

    }
}
