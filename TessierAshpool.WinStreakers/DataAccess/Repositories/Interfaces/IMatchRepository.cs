﻿using System.Collections.Generic;
using TessierAshpool.WinStreakers.Business.Entities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces
{
    public interface IMatchRepository
    {
        IEnumerable<Match> Matches { get; }
        Match FindById(int id);
    }
}
