﻿using System.Collections.Generic;
using TessierAshpool.WinStreakers.Business.Entities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces
{
    public interface ITeamRepository
    {
        IEnumerable<Team> Teams { get; }
        Team FindById(int id);
    }
}
