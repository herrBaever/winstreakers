﻿using System.Collections.Generic;
using TessierAshpool.WinStreakers.Business.Entities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces
{
    public interface IGameRepository
    {
        IEnumerable<Game> Games { get; }
        Game FindById(int id);
    }
}
