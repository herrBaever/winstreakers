﻿using System.Collections.Generic;
using TessierAshpool.WinStreakers.Business.Entities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces
{
    public interface IPlayerRepository
    {
        IEnumerable<Player> Players { get; }
        Player FindById(int id);
    }
}
