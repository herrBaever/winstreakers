﻿using System.Collections.Generic;
using TessierAshpool.WinStreakers.Business.Entities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces
{
    public interface ITeamsAndOddsRepository
    {
        IEnumerable<TeamOddsForMatch> TeamsAndOddsForMatches { get; }
        IEnumerable<Team> FindTeamsByMatch(int matchId);
        IEnumerable<Match> FindMatchesByTeam(int matchId);
    }
}
