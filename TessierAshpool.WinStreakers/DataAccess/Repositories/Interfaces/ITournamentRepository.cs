﻿using System;
using System.Collections.Generic;
using TessierAshpool.WinStreakers.Business.Entities;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces
{
    public interface ITournamentRepository
    {
        IEnumerable<Tournament> Tournaments { get; }
        IEnumerable<Tournament> FindTournamentsInTimeRange(DateTime startTime, DateTime endTime);
        IEnumerable<Tournament> FindTournamentsInRegion(Region region);
    }
}
