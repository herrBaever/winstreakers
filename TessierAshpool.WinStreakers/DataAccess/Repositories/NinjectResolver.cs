﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Ninject;
using TessierAshpool.WinStreakers.DataAccess.Repositories.Concrete;
using TessierAshpool.WinStreakers.DataAccess.Repositories.Interfaces;

// <author>Jonas Benzon Illum</author>

namespace TessierAshpool.WinStreakers.DataAccess.Repositories
{
    public class NinjectResolver : IDependencyResolver
    {

        private IKernel kernel;

        public NinjectResolver(IKernel kernel)
        {
            this.kernel = kernel;
            AddBindings();
        }

        private void AddBindings()
        {
            kernel.Bind<IGameRepository>().To<GameRepository>();
            kernel.Bind<IMatchRepository>().To<MatchRepository>();
            kernel.Bind<IPlayerRepository>().To<PlayerRepository>();
            kernel.Bind<ITeamRepository>().To<TeamRepository>();
            kernel.Bind<ITeamsAndOddsRepository>().To<TeamsAndOddsRepository>();
            kernel.Bind<ITournamentRepository>().To<TournamentRepository>();
        }

        public object GetService(Type service)
        {
            return kernel.TryGet(service);
        }

        public IEnumerable<object> GetServices(Type service)
        {
            return kernel.GetAll(service);
        }

    }
}